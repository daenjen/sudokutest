using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    /// <summary>
    /// UPDATE THE BOX NUMBER
    /// </summary>
    /// <param name="number"></param>
    public delegate void UpdateBoxNumber(int number);
    public static event UpdateBoxNumber OnBoxUpdateNumber;

    public static void UpdateBoxNumberMethod(int number)
    {
        if (OnBoxUpdateNumber != null)
            OnBoxUpdateNumber(number);
    }

    /// <summary>
    /// SELECT THE BOX
    /// </summary>
    /// <param name="boxIndex"></param>
    public delegate void BoxSelect(int boxIndex);
    public static event BoxSelect OnUpdateBoxSelect;

    public static void UpdateBoxSelectMethod(int boxIndex)
    {
        if (OnUpdateBoxSelect != null)
            OnUpdateBoxSelect(boxIndex);
    }

    /// <summary>
    /// ON WRONG NUMBER
    /// </summary>
    public delegate void WrongNumber();
    public static event WrongNumber OnWrongNumber;

    public static void OnWrongNumberMethod()
    {
        if (OnWrongNumber != null)
            OnWrongNumber();
    }

    /// <summary>
    /// BOARD COMPLETE
    /// </summary>
    public delegate void BoardComplete();
    public static event BoardComplete OnBoardComplete;

    
    public static void OnBoardCompleteMethod()
    {
        if (OnBoardComplete != null)
            OnBoardComplete();
    }

    /// <summary>
    /// CHECK BOARD COMPLETE
    /// </summary>
    public delegate void CheckBoard();
    public static event CheckBoard OnCheckBoardComplete;

    public static void CheckIfBoardCompleteMethod()
    {
        if (OnCheckBoardComplete != null)
            OnCheckBoardComplete();

    }

    /// <summary>
    /// GIVE A HINT
    /// </summary>
    public delegate void GiveHint();
    public static event GiveHint OnGiveHint;

    public static void GiveHintMethod()
    {
        if (OnGiveHint != null)
            OnGiveHint();
    }
}
