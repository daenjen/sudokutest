using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("Stats")]
    public int actualLives = 3;

    public float maxScore = 0;

    [Header("GameObjects")]
    public GameObject gameOverPanel;
    public GameObject winGamePanel;

    [Header("Texts")]
    public Text textMainMenuScore;
    public Text textLivesValue;
    public Text textTimerValue;
    public Text textScoreValueGameOver;
    public Text textScoreValueWin;

    [Header("References")]
    public SudokuGrid myGrid;

    [HideInInspector]
    public bool isEasy, isNormal, isHard, isVeryHard, isStart = false;

    //locals
    private float actualScore;
    private float timerCount = 0.0f;

    void Start()
    {
        if (instance == null)
            instance = this;
    }

    void Update()
    {
        textLivesValue.text = actualLives.ToString();

        if (isStart)
            Timer();
    }

    public void BoardComplete()
    {
        winGamePanel.SetActive(true);
    }

    public void WrongNumber()
    {
        actualLives--;

        CheckGameOver();
    }

    public void CheckGameOver()
    {
        if (actualLives <= 0)
        {
            isStart = false;

            gameOverPanel.SetActive(true);

            //Set final score
            float finalScore = maxScore - timerCount;
            actualScore = Mathf.Round(finalScore);
            textScoreValueGameOver.text = actualScore.ToString();
            textScoreValueWin.text = actualScore.ToString();
        }
    }

    public void Timer()
    {
        timerCount += Time.deltaTime;
        textTimerValue.text = Mathf.Round(timerCount).ToString();
    }

    public void EnableObject(GameObject obj)
    {
        obj.SetActive(true);
    }

    public void DisableObject(GameObject obj)
    {
        obj.SetActive(false);
    }

    private void OnEnable()
    {
        EventManager.OnWrongNumber += WrongNumber;

        EventManager.OnBoardComplete += BoardComplete;
    }

    private void OnDisable()
    {
        EventManager.OnWrongNumber -= WrongNumber;

        EventManager.OnBoardComplete -= BoardComplete;
    }

    #region Buttons
    public void ButtonEasy()
    {
        isEasy = true;
        isStart = true;

        myGrid.CreateGrid();
    }

    public void ButtonNormal()
    {
        isNormal = true;
        isStart = true;

        myGrid.CreateGrid();
    }

    public void ButtonHard()
    {
        isHard = true;
        isStart = true;

        myGrid.CreateGrid();
    }

    public void ButtonVeryHard()
    {
        isVeryHard = true;
        isStart = true;

        myGrid.CreateGrid();
    }

    public void ButtonReturnMainMenu()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void ButtonHint()
    {
        EventManager.GiveHintMethod();
    }

    public void ButtonQuit()
    {
        Application.Quit();
    }
    #endregion
}
