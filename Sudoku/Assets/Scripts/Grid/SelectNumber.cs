using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectNumber : Selectable, IPointerClickHandler, ISubmitHandler, IPointerUpHandler, IPointerExitHandler
{
    [Header("Value")]
    public int numberValue = 0;

    public void OnPointerClick(PointerEventData eventData)
    {
        EventManager.UpdateBoxNumberMethod(numberValue);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        
    }
}
