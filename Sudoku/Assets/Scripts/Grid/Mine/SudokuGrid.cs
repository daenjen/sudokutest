using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SudokuGrid : MonoBehaviour
{
    [Header("Stats")]
    public int rows = 0;
    public int cols = 0;
    public int boxIndex = 0;

    public float boxOffSet = 0.0f;
    public float boxScale = 1.0f;

    [Header("GameObjects")]
    public GameObject gridBox;

    [Header("Positions")]
    public Vector2 startPosition = new Vector2(0.0f, 0.0f);

    //Locals
    private int colIndex = 0;
    private int rowIndex = 0;
    private int puzzleDifficultyData = -1;

    private GameManager myManager;

    private Vector2 offSet = new Vector2();

    private List<GameObject> gridBoxList = new List<GameObject>();

    public void CreateGrid()
    {
        SpawnGridBox();
        SetBoxPosition();
    }

    private void SpawnGridBox()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                //Add grid boxes
                gridBoxList.Add(Instantiate(gridBox) as GameObject);
                //Get box index
                gridBoxList[gridBoxList.Count - 1].GetComponent<SudokuGridBox>().SetIndex(boxIndex);
                //Set grid boxes child
                gridBoxList[gridBoxList.Count - 1].transform.SetParent(this.transform);
                //Set grid boxes scale
                gridBoxList[gridBoxList.Count - 1].transform.localScale = new Vector3(boxScale, boxScale, boxScale);

                boxIndex++;
            }
        }
    }

    private void SetBoxPosition()
    {
        //Get box transform
        var boxRect = gridBoxList[0].GetComponent<RectTransform>();
        //Add the box x,y a offset 
        offSet.x = boxRect.rect.width * boxRect.transform.localScale.x + boxOffSet;
        offSet.y = boxRect.rect.height * boxRect.transform.localScale.y + boxOffSet;

        //Set rows, cols
        foreach (GameObject box in gridBoxList)
        {
            if (colIndex + 1 > cols)
            {
                rowIndex++;
                colIndex = 0;
            }

            //Calculate offset rows and cols
            var posX = offSet.x * colIndex;
            var posY = offSet.y * rowIndex;

            //Set boxes position
            box.GetComponent<RectTransform>().anchoredPosition = new Vector2(startPosition.x + posX, startPosition.y + posY);
            colIndex++;
        }

        SelectDifficulty();
    }

    private void SetGridIndex(string difficulty)
    {
        //Get random puzzle in the same difficulty
        var puzzleList = PuzzleManager.instance.SudokuDictionary[difficulty];
        puzzleDifficultyData = Random.Range(0, puzzleList.Count);

        //Set the difficulty choose
        var puzzleIndex = PuzzleManager.instance.SudokuDictionary[difficulty][puzzleDifficultyData];
        SetPuzzleDifficulty(puzzleIndex);
    }

    private void SetPuzzleDifficulty(PuzzleManager.PuzzleData puzzleIndex)
    {
        for (int index = 0; index < gridBoxList.Count; index++)
        {
            //Fill numbers
            gridBoxList[index].GetComponent<SudokuGridBox>().SetNumber(puzzleIndex.puzzleIncomplete[index]);
            //Fill correct numbers
            gridBoxList[index].GetComponent<SudokuGridBox>().SetCorrectNumber(puzzleIndex.puzzleComplete[index]);
            //Set default tiles
            gridBoxList[index].GetComponent<SudokuGridBox>().SetDefaultValue(puzzleIndex.puzzleIncomplete[index] != 0 && puzzleIndex.puzzleIncomplete[index] == puzzleIndex.puzzleComplete[index]);
        }
    }

    private void SelectDifficulty()
    {
        //Store game manager booleans
        bool easy = GameManager.instance.isEasy;
        bool normal = GameManager.instance.isNormal;
        bool hard = GameManager.instance.isHard;
        bool veryhard = GameManager.instance.isVeryHard;

        //Select grid by difficulty
        if (easy == true)
            SetGridIndex("Easy");
        else if (normal == true)
            SetGridIndex("Normal");
        else if (hard == true)
            SetGridIndex("Hard");
        else if (veryhard == true)
            SetGridIndex("VeryHard");
    }

    public void CheckBoardComplete(int number)
    {
        foreach (var box in gridBoxList)
        {
            var boxComponent = box.GetComponent<SudokuGridBox>();
            if(boxComponent.GetCorrectNumber() == false)
            {
                return;
            }
        }

        EventManager.OnBoardCompleteMethod();
    }

    public void SolveBoard()
    {
        var myBoxIndex = new List<int>();
        for (int index = 0; index < gridBoxList.Count; index++)
        {
            var boxComponent = gridBoxList[index].GetComponent<SudokuGridBox>();
            if(boxComponent.GetDefaultValue() == false)
            {
                myBoxIndex.Add(index);
            }
        }

        //Skip first box
        if (myBoxIndex.Count == 0)
            return;

        var randomIndex = Random.Range(0, myBoxIndex.Count);
        var randomBoxIndex = myBoxIndex[randomIndex];
        gridBoxList[randomBoxIndex].GetComponent<SudokuGridBox>().SetValueHint();
    }

    private void OnEnable()
    {
        EventManager.OnBoxUpdateNumber += CheckBoardComplete;
    }

    private void OnDisable()
    {
        EventManager.OnBoxUpdateNumber -= CheckBoardComplete;
    }
}
