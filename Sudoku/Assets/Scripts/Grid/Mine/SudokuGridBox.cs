using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SudokuGridBox : Selectable, IPointerClickHandler, ISubmitHandler, IPointerUpHandler, IPointerExitHandler
{
    [Header("GameObjects")]
    public GameObject textNumber;

    //Sets
    public void SetIndex(int index) { boxIndex = index; }
    public void SetCorrectNumber(int index) { correctNumber = index; }
    public void SetDefaultValue(bool isdefault) { isDefaultValue = isdefault; }

    //Gets
    public bool GetSelect() { return canSelect; }
    public bool GetDefaultValue() { return isDefaultValue; }
    public bool GetCorrectNumber() { return myNumber == correctNumber; }

    //locals
    private int myNumber = 0;
    private int boxIndex = -1;
    private int correctNumber = 0;

    private bool canSelect = false;
    private bool isDefaultValue = false;

    public void DisplayText()
    {
        if (myNumber <= 0)
        {
            textNumber.GetComponent<Text>().text = "";
        }
        else
        {
            textNumber.GetComponent<Text>().text = myNumber.ToString();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        canSelect = true;
        EventManager.UpdateBoxSelectMethod(boxIndex);
    }

    public void OnSubmit(BaseEventData eventData)
    {

    }

    public void SetNumber(int number)
    {
        //Set the number
        myNumber = number;
        DisplayText();
    }

    public void SetValueHint()
    {
        //Hint the correct number
        SetBoxNumber(correctNumber);
    }

    public void SetCorrectNumber()
    {
        //Give the correct number
        myNumber = correctNumber;
        DisplayText();
    }

    public void OnBoxSelect(int box_Index)
    {
        if (boxIndex != box_Index)
            canSelect = false;
    }

    public void OnSetNumber(int number)
    {
        SetBoxNumber(number);
    }

    public void SetBoxNumber(int number)
    {
        if (canSelect && isDefaultValue == false)
        {
            SetNumber(number);

            //Set color
            if (myNumber != correctNumber)
            {
                var colors = this.colors;
                colors.normalColor = Color.red;
                this.colors = colors;

                EventManager.OnWrongNumberMethod();
            }
            else
            {
                isDefaultValue = true;
                var colors = this.colors;
                colors.normalColor = Color.white;
                this.colors = colors;
            }
        }

        //Check the board
        GameManager.instance.myGrid.CheckBoardComplete(number);
    }

    protected override void OnEnable()
    {
        EventManager.OnBoxUpdateNumber += OnSetNumber;
        EventManager.OnUpdateBoxSelect += OnBoxSelect;
        EventManager.OnGiveHint += SetValueHint;

    }
    protected override void OnDisable()
    {
        EventManager.OnBoxUpdateNumber -= OnSetNumber;
        EventManager.OnUpdateBoxSelect -= OnBoxSelect;
        EventManager.OnGiveHint -= SetValueHint;
    }
}
