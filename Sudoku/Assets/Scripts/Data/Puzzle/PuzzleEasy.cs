using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleEasy : PuzzleManager
{
    public static List<PuzzleData> getData()
    {
        List<PuzzleData> data = new List<PuzzleData>();

        data.Add(new PuzzleData(
            new int[81] { 0,0,0,0,9,7,0,0,0,0,0,0,8,0,0,5,0,0,0,9,0,2,0,5,0,0,7,5,0,0,0,0,8,1,0,6,8,0,
                0,0,3,0,0,0,2,4,0,1,7,0,0,0,0,8,1,0,0,4,0,2,0,7,0,0,0,3,0,0,1,0,0,0,0,0,0,3,6,0,0,0,0},

            new int[81] { 3,4,5,6,9,7,2,8,1,7,1,2,8,4,3,5,6,9,6,9,8,2,1,5,4,3,7,5,3,7,9,2,8,1,4,6,8,6,
                9,1,3,4,7,5,2,4,2,1,7,5,6,3,9,8,1,5,6,4,8,2,9,7,3,9,8,3,5,7,1,6,2,4,2,7,4,3,6,9,8,1,5}));

        return data;
    }
}
