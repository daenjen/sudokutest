using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public static PuzzleManager instance;

    public struct PuzzleData
    {
        public int[] puzzleIncomplete;
        public int[] puzzleComplete;

        public PuzzleData(int[] incomplete, int[] complete) : this()
        {
            this.puzzleIncomplete = incomplete;
            this.puzzleComplete = complete;
        }
    };

    public Dictionary<string, List<PuzzleData>> SudokuDictionary = new Dictionary<string, List<PuzzleData>>();

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        SudokuDictionary.Add("Easy", PuzzleEasy.getData());
        SudokuDictionary.Add("Normal", PuzzleNormal.getData());
        SudokuDictionary.Add("Hard", PuzzleHard.getData());
        SudokuDictionary.Add("VeryHard", PuzzleVeryHard.getData());
    }
}
