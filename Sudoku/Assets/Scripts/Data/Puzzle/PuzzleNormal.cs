using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleNormal : PuzzleManager
{
    public static List<PuzzleData> getData()
    {
        List<PuzzleData> data = new List<PuzzleData>();

        data.Add(new PuzzleData(
            new int[81] { 0,3,0,6,0,5,0,0,7,0,0,0,0,0,9,8,0,4,0,0,2,4,0,0,0,0,0,0,0,0,8,7,0,0,3,0,0,0,
                0,0,0,0,1,0,0,4,0,6,0,9,0,0,0,0,0,6,0,0,0,0,4,0,5,0,0,0,0,0,0,0,6,9,0,0,7,0,5,0,0,0,0},

            new int[81] { 8,3,4,6,1,5,2,9,7,6,7,5,3,2,9,8,1,4,9,1,2,4,8,7,6,5,3,5,2,1,8,7,4,9,3,6,7,9,
                3,5,6,2,1,4,8,4,8,6,1,9,3,5,7,2,1,6,9,7,3,8,4,2,5,3,5,8,2,4,1,7,6,9,2,4,7,9,5,6,3,8,1}));

        return data;
    }
}
