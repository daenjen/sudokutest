using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleHard : PuzzleManager
{
    public static List<PuzzleData> getData()
    {
        List<PuzzleData> data = new List<PuzzleData>();

        data.Add(new PuzzleData(
            new int[81] { 0,3,0,4,0,0,0,0,0,9,0,0,0,0,2,6,0,0,0,0,0,0,3,8,0,1,0,0,0,0,0,0,6,0,5,9,1,0,
                0,0,5,0,0,0,8,2,5,0,3,0,0,0,0,0,0,4,0,5,7,0,0,0,0,0,0,6,8,0,0,0,0,7,0,0,0,0,0,4,0,2,0},

            new int[81] { 6,3,1,4,9,5,7,8,2,9,8,5,7,1,2,6,3,4,4,2,7,6,3,8,9,1,5,8,7,3,1,4,6,2,5,9,1,6,
                4,2,5,9,3,7,8,2,5,9,3,8,7,4,6,1,3,4,2,5,7,1,8,9,6,5,9,6,8,2,3,1,4,7,7,1,8,9,6,4,5,2,3}));

        return data;
    }
}
